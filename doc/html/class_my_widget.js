var class_my_widget =
[
    [ "MyWidget", "class_my_widget.html#a4a9d659321b8e86eef35b115a767a991", null ],
    [ "~MyWidget", "class_my_widget.html#a47499f400b6a34b44ad9727f31d0948f", null ],
    [ "ai", "class_my_widget.html#ac52225ab1c8e60c6f774e97a62ae865d", null ],
    [ "gamestep", "class_my_widget.html#ac8da8cba1365577be55228866d2ea26f", null ],
    [ "get_points", "class_my_widget.html#ac00ac93ae147fb51b4a3b8096b7f10d5", null ],
    [ "paintEvent", "class_my_widget.html#a2eeeb2b1c12e032b1e99427f7991bb02", null ],
    [ "b", "class_my_widget.html#a56a324797198e7264c6b734fc3a1158b", null ],
    [ "gamefield_x", "class_my_widget.html#afdcdce66bd092c0262a0b77c6113888e", null ],
    [ "gamefield_y", "class_my_widget.html#a31580d4b85b73ce537ab197cb4983cb0", null ],
    [ "historia", "class_my_widget.html#a3b86d9742e0d7756473f7e70148ad1f3", null ],
    [ "player1", "class_my_widget.html#a787c7254a5471f8b73bc52b12391c03f", null ],
    [ "player2", "class_my_widget.html#a4361e5f4d0f43f2fe9643bd07c67fc2f", null ],
    [ "points1", "class_my_widget.html#a01d813ebd7b88e591a359bf5ccafb392", null ],
    [ "points2", "class_my_widget.html#a8a0a4814dd9a1b3db77e8eb06ca47e9f", null ],
    [ "pom1", "class_my_widget.html#a9641e4496882038dc4b5470fbe0481db", null ],
    [ "pom2", "class_my_widget.html#a09c01b7eca16784ce925bdd456f7e69b", null ],
    [ "pom3", "class_my_widget.html#a1def8a11abd2a6a9ecf613b1fb347a88", null ],
    [ "speed", "class_my_widget.html#a1e0eb639218ad85e8416d79b9d0e821a", null ],
    [ "starttime", "class_my_widget.html#a2f2db93fbb2c60a6242558cb5f403dab", null ]
];