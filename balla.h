#ifndef BALLA_H
#define BALLA_H
/*!
 * \file
 * \brief Plik nagłówkowy z definicją klasy balla
 */

/*!
 * \brief Klasa przedstawiająca wszystkie właściwosci piłki
 *
 * Klasa modeluje pojęcie piłki w grze o zadanym rozmiarze, pozycji i orientacji w przestrzeni
 */
class balla
{
    /*!
     * \brief Pozycja x
     * Pole zawiera aktualną współrzędną x piłki
     */
    int posx=50;
    /*!
     * \brief Pozycja y
     * Pole zawiera aktualną współrzędną y piłki
     */
    int posy=80;
    /*!
     * \brief Promień
     * Pole zawiera wartość promienia ka podstawie której rysowana jest piłka na polu gry i rozważane są kolizje piłki z innymi obiektami
     */
    int r=10;
    /*!
     * \brief Orientacja
     * Pole zawiera orientację piłki w przestrzeni (0-360 deg)
     */
    int dirrection=45;

public:
    /*!
     * \brief Domyślny konstruktor klasy balla
     */
    balla();

    /*!
     * \brief Domyślny destruktor klasy balla
     */
 //   ~balla();

    /*!
     * \brief Nadaje wartość parametrowi promienia
     * Zastępuje warotść pola r wartośćią new_r
     * \param [in] new_r -- nowa wartość promienia
     */
    void set_r(int new_r);

    /*!
     * \brief Nadaje wartość pozycji y
     * Zastępuje warotść pola r wartośćią posy
     * \param [in] new_y -- nowa wartość pozycji y
     */
    void set_posy(int new_y);
    /*!
     * \brief Nadaje wartość pozycji x
     * Zastępuje warotść pola r wartośćią posx
     * \param [in] new_x -- nowa wartość pozycji x
     */
    void set_posx(int new_x);

    /*!
     * \brief Nadaje wartość parametrowi dir
     * Zasępuje aktualną wartość pola dir wartośćią new_dir
     * \param [in] new_dir -- nowa wartość orientacji
     */
    void set_dir(int new_dir);

    /*!
     * \brief Metoda zwracająca wartość pola orientacji piłki
     * \return [int] -- wartość pola dir z zakresu 0-360 [stopni]
     */
    int get_dir();
    /*!
     * \brief Metoda zwracająca pozycję piłki na osi x
     * \return [int] -- wartość pola posx z zakresu 0-100
     */
    int get_x();
    /*!
     * \brief Metoda zwracająca pozycję piłki na osi y
     * \return [int] -- wartość pola posy z zakresu 0-100
     */
    int get_y();
    /*!
     * \brief Metoda zwracająca wartość pola primienia
     * \return [int] -- wartość promienia piłki w pikselach
     */
    int get_r();
};



#endif // BALLA_H
