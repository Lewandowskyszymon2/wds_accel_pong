#include<Wire.h>    
#include<SoftWire.h>     
#define ACC_ADDRESS 0x4C
#define LED PC13

boolean toggle = false;
int c,x,y,z,xs,ys,zs, temp;
char data[50];// Tablica przechowujaca wysylana wiadomosc.
uint16_t rozm = 0; // Rozmiar wysylanej wiadomosci ++cnt; // Zwiekszenie licznika wyslanych wiadomosci.

void setup() {
  // put your setup code here, to run once:
pinMode(LED, OUTPUT);
Serial.begin(115200);
Wire.begin();
Wire.beginTransmission(ACC_ADDRESS);
Wire.write(0x07);
Wire.write(0x01);
Wire.endTransmission();

}

void loop() {
  // put your main code here, to run repeatedly:
  toggle=!toggle;
  digitalWrite(LED,toggle);
Wire.beginTransmission(ACC_ADDRESS);
Wire.write(0x00);
Wire.endTransmission();
Wire.requestFrom(ACC_ADDRESS,3);
if (Wire.available()<=3)
x=Wire.read();
y=Wire.read();
z=Wire.read();
xs=x&0x40;
if (xs==0){
xs=(x>>5);
if (xs==1){
xs=x&(1+2+4+8+16);
xs*=-1;
}
else xs=x&(1+2+4+8+16);
Serial.print("X ");
Serial.print(xs,DEC);
}
//******************************************************************************************************************
ys=y&0x40;
if (ys==0){
ys=(y>>5);
if (ys==1){
ys=y&(1+2+4+8+16);
ys*=-1;
}
else ys=y&(1+2+4+8+16);
Serial.print(" ");
Serial.print(ys,DEC);
}
//******************************************************************************************************************
zs=z&0x40;
if (zs==0){
zs=(z>>5);
if (zs==1){
zs=z&(1+2+4+8+16);
zs*=-1;
}
else zs=z&(1+2+4+8+16);
Serial.print(" ");
Serial.print(zs,DEC);
}
//******************************************************************************************************************

/*        DODANIE SUMY CRC NA KOŃCU 
rozm = sprintf(data, "X %d %d %d",xs,ys,zs);
c=crc8(data,rozm);
Serial.print("crc:");
Serial.print(c);
*/
Serial.println(" ");
delay(100); 
}


uint8_t crc8( char *addr, uint8_t len) {

  uint8_t bitarray[408];
  uint8_t i,j,t1,t2,tc1,tc2;
    uint8_t crc=0;
    uint8_t key[8];
    uint8_t operlen=0;
    
    char temp[50];// Tablica przechowujaca wysylana wiadomosc.

      /*
      for (uint8_t i=0; i<len;i++) {
         uint8_t inbyte = addr[i];
         for (uint8_t j=0;j<8;j++) {
             uint8_t mix = (crc ^ inbyte) & 0x01;
             crc >>= 1;
             if (mix)
                crc ^= 0x8C;
         inbyte >>= 1;
      }
    }*/
      //rozłożenie ciągu data na poszczególne bity
      for (i=0;i<rozm;i++){
        temp[i]=data[i];

        for (j=0;j<8;j++){
          t1=7+8*i-j;
          t2=(data[i]>>j)%2;
          bitarray[t1]=t2;
        }
      }
      //wyznaczenie klucza
      for (j=0;j<8;j++){
        t1=7-j;     //idąc od najmniej znaczącego bitu
        t2=(len>>j)%2;  //wyciągamy resztę z dzielenia przeez 2
        key[t1]=t2;   //wpisujemy do tabeli
        if (t2==1)  tc1=j;//jeżeli bit był dodatni to zwiększamy rząd klucza
        //tc1=rząd klucza(liczba bitów klucza z przykładu)
      }


      //wyznaczanei reszty z dzielenia wyrazu(całego pakietu danych) przez klucz
      for (i=0;i<len*8;i++){    //iteracja po całym wyrazie
        if(bitarray[i]==1)    //jeżeli aktualny bit jest 1
          for(j=0;j<tc1;j++){ //iteracja po długości wyrazu (tyle znaków ile ma klucz)
            if (bitarray[i+j]==key[j]) bitarray[i+j]=0; else bitarray[i+j]=1; //XOR
          }
      }

      //wyznaczanie wartości sumy kontrolnej
      for (i=0;i<tc1-1;i++)
        tc2=2*tc2 + bitarray[len*8+i];
crc=tc2;

   return crc;
}
