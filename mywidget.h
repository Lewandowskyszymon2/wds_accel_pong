#ifndef MYWIDGET_H
#define MYWIDGET_H

/*!
 * \file
 * \brief Plik nagłówkowy z definicją klasy mywidget
 */

#include <QWidget>
#include <QPainter>
#include <QPen>
#include <QDebug>
#include "paletta.h"
#include "balla.h"
#include "mainwindow.h"
#include <list>
#include <array>


/*!
 * \brief Klasa obsługująca wyświetlacz
 *
 * klasa odpowiedzialna za wyświetlanie treści na głównym panemu okna głównego oraz przechowywyanie informacji o grze (zawiera w sobie obiekty klas podrzędnych posiadające m. in. informacje o pozycji obiektów)
 */
class MyWidget : public QWidget
{
Q_OBJECT

public:
    /*!
     * \brief Domyślny konstruktor klasy MyWidget
     * \param parent -- wskaźnik na rodzica
     */
    MyWidget(QWidget *parent = nullptr);


    /*!
     * \brief Domyślny destruktor klasy MyWidget
     */
    ~MyWidget();

    /*!
     * \brief Zmienna przechowywująca wartość czasu początkowego
     */
    int starttime;
    /*!
     * \brief Zmienna przechowywujaca wymiary pola gry(do rysowania)
     */
    int gamefield_x;
    /*!
     * \brief Zmienna przechowywujaca wymiary pola gry(do rysowania)
     */
    int gamefield_y;
    /*!
     * \brief Liczniki punktów gracza pierwszego
     */
    int points1=0;
    /*!
     * \brief Liczniki punktów gracza drugiego
     */
    int points2=0;
    /*!
     * \brief Prędkość przemieszczania się piłki (długość korku wykonywanego przez piłkę)
     */
    int speed=2;

    /*!
     * \brief Lista do przetrzymywania historii parametrów dla grafów
     */
    std::list <std::array <int,6>> historia;
    /*!
     * \brief Paletka pierwszego gracza
     */
    paletta player1;
    /*!
     * \brief Paletka drugiego gracza
     */
    paletta player2;
    /*!
     * \brief Pomiar siły w osi x kontrolera
     */
    int pom1;
    /*!
     * \brief Pomiar siły w osi y kontrolera
     */
    int pom2;
    /*!
     * \brief Pomiar siły w osi z kontrolera
     */
    int pom3;
    /*!
     * \brief Piłka
     */
    balla b;
    /*!
     * \brief Metoda zwracająca aktualną "pongtację"
     * \return QString -- string z punktacja
     */
    QString get_points();
    /*!
     * \brief Metoda odpowiedizalna z awykrywanie kolizji, odpowiednie reakcje na nia oraz krok gry
     */
    void gamestep();
    /*!
     * \brief Metoda sterująca paletką na podstwie pozycji piłki
     * \param [in] player -- wskaznik na paletkę którą ma sterować AI
     */
    void ai(paletta& player);
protected:
    /*!
     * \brief Wykonanie procedury rysowania treści widgetu
     * \param [in] event
     */
    void paintEvent(QPaintEvent *event);
};



#endif // MYWIDGET_H
