#ifndef PALETTA_H
#define PALETTA_H

/*!
 * \file
 * \brief Plik nagłówkowy z definicją klasy paletta
 */

#include <QObject>

/*!
 * \brief Klasa przedstawiająca wszystkie właściwosci paletki graczy
 *
 *  Klasa odpowiedzialna za przwchowywanie informacji o paletce
 */
class paletta : public QObject
{
    Q_OBJECT
public:
    explicit paletta(QObject *parent = nullptr);

    /*!
     * \brief Nadaje wartość parametrowi w paletki
     * Ustawia szerokość paletki na zadaną wielkosć
     * \param [in] new_w -- nowa szerokość paletki w pixelach
     */
    void set_w(int new_w);
    /*!
     * \brief Nadaje wartość parametrowi h paletki
     * Ustawia wysokość paletki na zadaną wielkosć
     * \param [in] new_h -- nowa wysokość paletki w pixelach
     */
    void set_h(int new_h);
    /*!
     * \brief Nadaje wartość parametrowi posy paletki
     * Ustawia współrzędną y paletki na zadaną wartość
     * \param [in] new_y -- nowa współrzędna y paletki z zakresu [0-100]
     */
    void set_posy(int new_y);
    /*!
     * \brief Nadaje wartość parametrowi posy paletki
     * Ustawia współrzędną x paletki na zadaną wartość
     * \param [in] new_x -- nowa współrzędna x paletki z zakresu [0-100]
     */
    void set_posx(int new_x);

    /*!
     * \brief Metoda zwracająca pozycję paletki na osi x
     * \return [int] --pozycja na osi x
     */
    int get_x();
    /*!
     * \brief Metoda zwracająca pozycję paletki na osi y
     * \return [int] --pozycja na osi y
     */
    int get_y();
    /*!
     * \brief Metoda zwracająca szerokosć paletki
     * \return [int] --szerokość paletki
     */
    int get_w();
    /*!
     * \brief Metoda zwracająca wysokość paletki
     * \return [int] --wysokość paletki
     */
    int get_h();


signals:

private:
    /*!
     * \brief Pozycja x paletki
     * Pole zawiera pozycję x paletki
     */
    int posx=0;
    /*!
     * \brief Pozycja y paletki
     * Pole zawiera pozycję y paletki
     */
    int posy=0;
    /*!
     * \brief Szerokość paletki
     * Pole zawiera szerokość paletki na podstawie której rysowana jest paletka w grze oraz rozpoznawana jest kolizja z piłką
     */
    int w=1;
    /*!
     * \brief Wysokość paletki
     * Pole zawiera wysokosć paletki na podstawie której rysowana jest paletka w grze oraz rozpoznawana jest kolizja z piłką
     */
    int h=10;

};


#endif // PALETTA_H
