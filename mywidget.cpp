/*!
 * \file
 * \brief Plik źródłowy z rozwinięciem metod klasy MyWidget
 */

#include "mywidget.h"
#include "paletta.h"
#include "mainwindow.h"
#include <windows.h>
#include <math.h>
#include <cstdlib>
#include <time.h>
#include <string.h>
#include <array>
#include <list>

MyWidget::MyWidget(QWidget *parent)
{
//    player1.set_pos((player1.get_w()));
//    player2.set_posx((gamefield_x-player2.get_w()));
    player1.set_posx(0);
    player2.set_posx(100);
    time(NULL);
}

MyWidget::~MyWidget()
{

}

QString MyWidget::get_points()
{
    std::string tempstr =std::string("punktacja:")+std::to_string(points1)+std::string(" ")+std::to_string(points2);
    return QString::fromStdString(tempstr);
}

void MyWidget::paintEvent(QPaintEvent *event)
{
    int temp1,temp2;
    QPen Pen;
   // int x;
   // int y;
    //x=this->width();
    //y=this->height();
    //qDebug()()<<counter++;
    QPainter painter(this);
    //x=gamefield_x;
    //y=gamefield_y;
    Sleep(50);
    temp1=(0);                                          //ustawienie w osi x lewej  paletki
    temp2=(gamefield_x - player2.get_w()*gamefield_x/100);    //ustawienie w osi x prawej paletki



    Pen.setColor(Qt::green);
    painter.setPen(Pen);
    painter.setBrush(Qt::gray);
    painter.drawRect(QRect(0,0,gamefield_x,gamefield_y));//ramka
    Pen.setColor(Qt::red);
    painter.setPen(Pen);
    painter.setBrush(Qt::red);
    painter.drawRect(QRect(temp1,(gamefield_y-player1.get_h()) * player1.get_y()/100,player1.get_w()*gamefield_x/100,player1.get_h()*gamefield_y/100));//lewy
    Pen.setColor(Qt::blue);
    painter.setPen(Pen);
    painter.setBrush(Qt::blue);
    painter.drawRect(QRect(temp2,(gamefield_y-player2.get_h()) * player2.get_y()/100,player2.get_w()*gamefield_x/100,player2.get_h()*gamefield_y/100));//prawy
    Pen.setColor(Qt::black);
    painter.setPen(Pen);
    painter.setBrush(Qt::black);
    painter.drawEllipse((gamefield_x-b.get_r())*b.get_x()/100,(gamefield_y-b.get_r())*b.get_y()/100,b.get_r(),b.get_r());


    //painter.drawRect(QRect(100,100,300,300));

}



void MyWidget::gamestep(){
int fx,fy;
int temp=0;

std::array <int,6> kolejnyArray;
double dectorad=3.14/180;
double aspx =(b.get_dir()*dectorad);
double aspy =(b.get_dir()*dectorad);
    fx=b.get_x()+speed*(cos(aspx));
    fy=b.get_y()+speed*(sin(aspy));
if (fy>100){                                            //odbicie od górnej krawędzi
    if (b.get_dir()>90) b.set_dir((b.get_dir()+90)%360);
    else b.set_dir((b.get_dir()+270)%360);
    fy=99;
}
if (fy<0){                                              //odbicie od dolnej krawędzi
    if (b.get_dir()>180){
    if (b.get_dir()>270) b.set_dir((b.get_dir()+90)%360);
    else b.set_dir((b.get_dir()+270)%360);
    fy=0;
    }
}

if (abs(player2.get_x()-b.get_x())<=1){                        //odbicie lub pudło z prawej
    if (b.get_dir()<270&&b.get_dir()>0)   b.set_dir((b.get_dir()+90)%360);
    else                                b.set_dir((b.get_dir()+270)%360);
    if (abs(player2.get_y()-b.get_y())<player2.get_h()){
    fx=(player2.get_x()-3);
    qDebug()<<"odbicie z prawej";
    }
    else{
    qDebug()<<"pudło z prawej";
    points1++;
    //b.set_dir(b.get_dir()+180);         //obróć trajektorię o 180*
    while (temp %90<10||temp %90>79){
    temp=rand() % 360;}
    b.set_dir(temp);
    fx=(50);
    fy=(50);
    qDebug()<<"punktacja:"<<points1<<" "<<points2;
    }

}

if (abs(player1.get_x()-b.get_x())<=1){                        //odbicie lub pudło z lewej
    if (b.get_dir()>180) b.set_dir((b.get_dir()+90)%360);
    else                b.set_dir((b.get_dir()+270)%360);
    if (abs(player1.get_y()-b.get_y())<player1.get_h()+4){
    fx=(player1.get_x()+player1.get_w()+2);
    qDebug()<<"odbicie z lewej";
    }
    else{
    qDebug()<<"pudło z lewej";
    points2++;
    //b.set_dir(b.get_dir()+180);         //obróć trajektorię o 180*
    while (temp%90<10||temp%90>79){
    temp=rand() % 360;}
    b.set_dir(temp);

    fx=(50);
    fy=(50);
    qDebug()<<"punktacja:"<<points1<<" "<<points2;

    }

}


    b.set_posx(fx);
    b.set_posy(fy);

    kolejnyArray[0]= QDateTime::currentMSecsSinceEpoch()-starttime;
    //qDebug()<<kolejnyArray[0];
    kolejnyArray[1]=player1.get_y();
    kolejnyArray[2]=player2.get_y();
    kolejnyArray[3]=b.get_x();
    kolejnyArray[4]=b.get_y();
    kolejnyArray[5]=b.get_dir();



this->historia.push_back(kolejnyArray);
}


void MyWidget::ai(paletta & player){


    player.set_posy(b.get_y());
}
