/*!
 * \file
 * \brief Plik źródłowy z rozwinięciem metod klasy Paletta
 */

#include "paletta.h"

paletta::paletta(QObject *parent) : QObject(parent)
{

}


void paletta::set_w(int new_w){
w=new_w;
}
void paletta::set_h(int new_h){
h=new_h;
}
void paletta::set_posy(int new_y){
posy=new_y;
}
void paletta::set_posx(int new_x){
posx=new_x;
}


int paletta::get_x(){
    return posx;
}

int paletta::get_y(){
    return posy;
}
int paletta::get_w(){
    return w;
}
int paletta::get_h(){
    return h;
}

