/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdio.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define ACC_ADRESS 0x4C
#define PI 3.14159265
//1001100
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t x,y,z;
//I2C_HandleTypeDef i2c;




static uint16_t cnt = 0; // Licznik wyslanych wiadomosci
uint8_t crcstring[50];
uint8_t sendstring[50];
uint8_t i,j,c,t1,t2,t3;
uint8_t temp[50];
uint8_t data[50];// Tablica przechowujaca wysylana wiadomosc.
uint8_t data2[50];// Tablica przechowujaca wysylana wiadomosc.
uint16_t size = 0; // Rozmiar wysylanej wiadomosci ++cnt; // Zwiekszenie licznika wyslanych wiadomosci.
uint16_t size2 = 0; // Rozmiar wysylanej wiadomosci ++cnt; // Zwiekszenie licznika wyslanych wiadomosci.

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
uint8_t I2C_read_reg(uint8_t reg);
uint8_t crc8( uint8_t*, uint8_t);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_TIM11_Init();
  MX_I2C2_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim11);

  x=1;
  y=2;
  z=3;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

			/*if (HAL_I2C_IsDeviceReady(&hi2c2, ACC_ADRESS,10, 10)==HAL_OK)
			{
			 	 x=I2C_read_reg(0x00);
			 	 ++cnt; // Zwiekszenie licznika wyslanych wiadomosci.
			 	 size = sprintf(data, "X %d %d %d\n\r",x,y,z); // Stworzenie wiadomosci do wyslania oraz przypisanie ilosci wysylanych znakow do zmiennej size.
			}
			else
			{
				 size = sprintf(data, "errorek\n\r"); // Stworzenie wiadomosci do wyslania oraz przypisanie ilosci wysylanych znakow do zmiennej size.
			}
*/
			//funkcja fakeująca pomiary
			++cnt; // Zwiekszenie licznika wyslanych wiadomosci.
			x=(100-(cnt%100));//10000* (sin (cnt*PI/180));
			y=(cnt%100);//10000* (sin (cnt*PI/180));

			size = sprintf(data, "X %d %d %d",x,y,z); // Stworzenie wiadomosci do wyslania oraz przypisanie ilosci wysylanych znakow do zmiennej size.
			//HAL_UART_Transmit(&huart2, data, size,1); // Rozpoczecie nadawania danych z wykorzystaniem przerwan



			c=crc8(data,size);

			size = sprintf(data, "X %d %d %d %d\n\r",x,y,z,c); // Stworzenie wiadomosci do wyslania oraz przypisanie ilosci wysylanych znakow do zmiennej size.
			if (HAL_UART_Transmit(&huart2, data, size,1)==HAL_OK);

				// Rozpoczecie nadawania danych z wykorzystaniem przerwan

			/*
			size = sprintf(data, "CRC %d\n\r",c); // Stworzenie wiadomosci do wyslania oraz przypisanie ilosci wysylanych znakow do zmiennej size.
			HAL_UART_Transmit(&huart2, data, size,1); // Rozpoczecie nadawania danych z wykorzystaniem przerwan



			size2 = sprintf(data2, "%d\n\r %d\n\r %d\n\r",x,y,z); // Stworzenie wiadomosci do wyslania oraz przypisanie ilosci wysylanych znakow do zmiennej size.
			HAL_UART_Transmit(&huart2, data2, size2,1); // Rozpoczecie nadawania danych z wykorzystaniem przerwan
			HAL_Delay(1000);

			*/





  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 64;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV16;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV16;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */


/*

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
 if (htim==&htim11)
 {
	 static uint16_t cnt = 0; // Licznik wyslanych wiadomosci
	 uint8_t data[50];// Tablica przechowujaca wysylana wiadomosc.
	 uint16_t size = 0; // Rozmiar wysylanej wiadomosci ++cnt; // Zwiekszenie licznika wyslanych wiadomosci.

	 ++cnt; // Zwiekszenie licznika wyslanych wiadomosci.
	 size = sprintf(data, "X %d %d %d\n\r",x,y,z); // Stworzenie wiadomosci do wyslania oraz przypisanie ilosci wysylanych znakow do zmiennej size.
	 HAL_UART_Transmit_IT(&huart2, data, size); // Rozpoczecie nadawania danych z wykorzystaniem przerwan

	 HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin); // Zmiana stanu pinu na diodzie LED
 }
}
*/


uint8_t I2C_read_reg(uint8_t reg)
{
	uint8_t value = 0;

HAL_I2C_Mem_Read(&hi2c2, ACC_ADRESS, reg, 1, &value, sizeof(value), HAL_MAX_DELAY);
//HAL_I2C_Mem_Read_DMA(&hi2c2, ACC_ADRESS, reg, 1, &value, sizeof(value));
//HAL_I2C_Mem_Read_IT(&hi2c2, ACC_ADRESS, reg, 1, &value, sizeof(value));
//HAL_I2C_Mem_Read(hi2c, DevAddress, MemAddress, MemAddSize, pData, Size, Timeout), DevAddress, MemAddress, MemAddSize, pData, Size, Timeout)


	if (HAL_OK==HAL_I2C_Mem_Read(&hi2c2, ACC_ADRESS, reg, 1, &value, sizeof(value), HAL_MAX_DELAY))
	return value;
	else return 127;
}

uint8_t crc8( uint8_t *addr, uint8_t len) {

	uint8_t bitarray[408];
	uint8_t i,j,t1,t2,tc1,tc2;
    uint8_t crc=0;
    uint8_t key[8];
    uint8_t operlen=0;

      /*
      for (uint8_t i=0; i<len;i++) {
         uint8_t inbyte = addr[i];
         for (uint8_t j=0;j<8;j++) {
             uint8_t mix = (crc ^ inbyte) & 0x01;
             crc >>= 1;
             if (mix)
                crc ^= 0x8C;
         inbyte >>= 1;
      }
    }*/
      //rozłożenie ciągu data na poszczególne bity
      for (i=0;i<size;i++){
      	temp[i]=data[i];

      	for (j=0;j<8;j++){
      		t1=7+8*i-j;
      		t2=(data[i]>>j)%2;
      		bitarray[t1]=t2;
      	}
      }
      //wyznaczenie klucza
    	for (j=0;j<8;j++){
    		t1=7-j;			//idąc od najmniej znaczącego bitu
    		t2=(len>>j)%2;	//wyciągamy resztę z dzielenia przeez 2
    		key[t1]=t2;		//wpisujemy do tabeli
    		if (t2==1)	tc1=j;//jeżeli bit był dodatni to zwiększamy rząd klucza
    		//tc1=rząd klucza(liczba bitów klucza z przykładu)
    	}


    	//wyznaczanei reszty z dzielenia wyrazu(całego pakietu danych) przez klucz
    	for (i=0;i<len*8;i++){		//iteracja po całym wyrazie
    		if(bitarray[i]==1)		//jeżeli aktualny bit jest 1
    			for(j=0;j<tc1;j++){	//iteracja po długości wyrazu (tyle znaków ile ma klucz)
    				if (bitarray[i+j]==key[j]) bitarray[i+j]=0;	else bitarray[i+j]=1;	//XOR
    			}
    	}

    	//wyznaczanie wartości sumy kontrolnej
    	for (i=0;i<tc1-1;i++)
    		tc2=2*tc2 + bitarray[len*8+i];
crc=tc2;

   return crc;
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
