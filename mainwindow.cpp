/*!
 * \file
 * \brief Plik źródłowy z rozwinięciem metod klasy MainWindow
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPen>
#include <qvector.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), m_serial(new QSerialPort(this)),ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->m_serial = new QSerialPort(this);
    starttime=QDateTime::currentMSecsSinceEpoch();
    this->ui->wyswietlacz->starttime=this->starttime;

    this->ui->plotwidget->setInteraction(QCP::iRangeDrag);
    this->ui->plotwidget->axisRect()->setRangeDrag(Qt::Horizontal);

    //p1y
    this->ui->plotwidget->addGraph();
    this->ui->plotwidget->graph(0)->setPen(QPen(Qt::red));
    this->ui->plotwidget->graph(0)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(0)->setScatterStyle(QCPScatterStyle::ssPlus);
    this->ui->plotwidget->graph(0)->setName("Player1_y");

    //p2y
    this->ui->plotwidget->addGraph();
    this->ui->plotwidget->graph(1)->setPen(QPen(Qt::blue));
    this->ui->plotwidget->graph(1)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);
    this->ui->plotwidget->graph(1)->setName("Player2_y");

    //bx
    this->ui->plotwidget->addGraph();
    this->ui->plotwidget->graph(2)->setPen(QPen(Qt::green));
    this->ui->plotwidget->graph(2)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(0)->setScatterStyle(QCPScatterStyle::ssStar);
    this->ui->plotwidget->graph(2)->setName("ball_x");

    //by
    this->ui->plotwidget->addGraph();
    this->ui->plotwidget->graph(3)->setPen(QPen(Qt::cyan));
    this->ui->plotwidget->graph(3)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(0)->setScatterStyle(QCPScatterStyle::ssNone);
    this->ui->plotwidget->graph(3)->setName("ball_y");

    //bdir
    this->ui->plotwidget->addGraph(this->ui->plotwidget->xAxis,this->ui->plotwidget->yAxis2);
    this->ui->plotwidget->graph(4)->setPen(QPen(Qt::magenta));
    this->ui->plotwidget->graph(4)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(0)->setScatterStyle(QCPScatterStyle::ssNone);
    this->ui->plotwidget->graph(4)->setName("ball_dirrection");

    //pom1
    this->ui->plotwidget->addGraph();
    this->ui->plotwidget->graph(5)->setPen(QPen(Qt::darkRed));
    this->ui->plotwidget->graph(5)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(5)->setScatterStyle(QCPScatterStyle::ssNone);
    this->ui->plotwidget->graph(5)->setName("accel_x");

    //pom2
    this->ui->plotwidget->addGraph();
    this->ui->plotwidget->graph(6)->setPen(QPen(Qt::darkGreen));
    this->ui->plotwidget->graph(6)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(6)->setScatterStyle(QCPScatterStyle::ssNone);
    this->ui->plotwidget->graph(6)->setName("accel_y");

    //pom3
    this->ui->plotwidget->addGraph();
    this->ui->plotwidget->graph(7)->setPen(QPen(Qt::darkBlue));
    this->ui->plotwidget->graph(7)->setLineStyle(QCPGraph::lsLine);
    this->ui->plotwidget->graph(7)->setScatterStyle(QCPScatterStyle::ssNone);
    this->ui->plotwidget->graph(7)->setName("accel_z");


    this->ui->plotwidget->yAxis2->setVisible(true);
    this->ui->plotwidget->legend->setVisible(true);
    this->ui->plotwidget->axisRect()->insetLayout()->setInsetAlignment(0,Qt::AlignLeft|Qt::AlignTop);
    //this->ui->plotwidget->legend->setFont();
    this->ui->plotwidget->yAxis->setRange(0, 100);
    this->ui->plotwidget->yAxis2->setRange(0,360);
    this->ui->plotwidget->xAxis->setLabel("time [s]");
    this->ui->plotwidget->yAxis->setLabel("position [units]");
    this->ui->plotwidget->yAxis2->setLabel("angle [deg]");


    timer.start(60, this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::addToLogs(QString message)
{
    QString currentDateTime = QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss");
    ui->textEditLog->append(currentDateTime + "\t" + message);

}



void MainWindow::on_pushButton_clicked()
{
}

void MainWindow::on_CloseButton_clicked()
{
    qDebug() << "Wcisnąłeś przycisk close";
}

void MainWindow::on_okButton_clicked()
{
    qDebug() << "Wcisnąłeś przycisk ok";
    this->addToLogs("Wcisnąłeś przycisk ok");
}


void MainWindow::on_searchButton_clicked()
{
    qDebug() << "Szukanie urządzeń";
    this->addToLogs("Szukanie urządzeń");
    QList<QSerialPortInfo> devices;
    devices = QSerialPortInfo::availablePorts();
    for(int i = 0; i < devices.count(); i++) {
      qDebug() << devices.at(i).portName() << devices.at(i).description();
      this->addToLogs(devices.at(i).portName());
      //portName=devices.at(i).portName();
      this->addToLogs(devices.at(i).description());
      ui->devicesBox->addItem(devices.at(i).portName() + " " + devices.at(i).description());
    }
}

void MainWindow::on_connectButton_clicked()
{
    if(ui->devicesBox->count() == 0) {
      this->addToLogs("Nie wykryto żadnych urządzeń!");
      return;
    }

    QString portName = ui->devicesBox->currentText().split(" ").first();
    m_serial->setPortName(portName);

    if(!m_serial->isOpen()) {
    if(m_serial->open(QIODevice::ReadWrite)) {

        // OTWÓRZ I SKONFIGURUJ PORT:
        m_serial->setBaudRate(QSerialPort::Baud115200);
        m_serial->setDataBits(QSerialPort::Data8);
        m_serial->setParity(QSerialPort::NoParity);
        m_serial->setStopBits(QSerialPort::OneStop);
        m_serial->setFlowControl(QSerialPort::NoFlowControl);
        connect(this->m_serial, SIGNAL(readyRead()), this, SLOT(readFromPort()));
      this->addToLogs("Otwarto port szeregowy.");
      mode=1;
      ui->connectButton->setEnabled(false);
      ui->disconnectButton->setEnabled(true);

    } else {
      this->addToLogs("Otwarcie portu szeregowego się nie powiodło!");
    }
    } else {
      this->addToLogs("Port już jest otwarty!");
      return;
    }

}

void MainWindow::on_disconnectButton_clicked()
{
    if(this->m_serial->isOpen()) {
      this->m_serial->close();
      this->addToLogs("Zamknięto połączenie.");
        mode=0;
        ui->connectButton->setEnabled(true);
        ui->disconnectButton->setEnabled(false);
    } else {
      this->addToLogs("Port nie jest otwarty!");
      return;
    }
}


void MainWindow::readFromPort() {

    if (mode==1){
  while(this->m_serial->canReadLine()) {
      QString line = this->m_serial->readLine();
    QRegExp rx("[ ]");//match space
    QStringList list= line.split(rx,QString::SkipEmptyParts);



    if (list.at(0)=="X")
    {
        //qDebug()<<"works";
        //qDebug()<<list;
        if (list.size()>4){
        for (int I = 0; I < 4; I++) {
            data[I]=list.at(I+1).toInt();
        }

        QString terminator = "\n\r";
        int pos = line.lastIndexOf(terminator);
        //qDebug() << line.left(pos);
        //qDebug()<<data[0]<<data[1]<<data[2]<<data[3];


        //this->addToLogs(line.left(pos));
    }

    }
    //tu wstaw kontrolę crc

    this->ui->wyswietlacz->pom1=data[0];
    this->ui->wyswietlacz->pom2=data[1];
    this->ui->wyswietlacz->pom3=data[2];
    if (data[0]>=0)//x>0
    {
        if (data[1]>0){

            this->ui->wyswietlacz->player1.set_posy(100-((data[1])*50/23));
            }
    }
        else{ //x<=0
        if (data[1]>0){
            this->ui->wyswietlacz->player1.set_posy(0+((data[1])*50/23));
            }
    }



  }
}

}
void MainWindow::paintEvent(QPaintEvent *event)
{




}


void MainWindow::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer.timerId()) {

        QVector <double> qv_d0;
        QVector <double> qv_d1;
        QVector <double> qv_d2;
        QVector <double> qv_d3;
        QVector <double> qv_d4;
        QVector <double> qv_d5;
        QVector <double> qv_d6;
        QVector <double> qv_d7;
        QVector <double> qv_d8;



        //qDebug()<<this->ui->tabWidget->height()<<this->ui->tabWidget->width();
        this->ui->wyswietlacz->gamefield_x=this->ui->tabWidget->width()-7;
        this->ui->wyswietlacz->gamefield_y=this->ui->tabWidget->height()-27;
        this->ui->plotwidget->resize(this->ui->tabWidget->width()-7,this->ui->tabWidget->height()-27);



        if (this->ui->tabWidget->currentIndex()==0||this->ui->tabWidget->currentIndex()==1)
        {
        this->ui->wyswietlacz->gamestep();
        this->ui->wyswietlacz->ai(this->ui->wyswietlacz->player2);
        if (mode==0)this->ui->wyswietlacz->ai(this->ui->wyswietlacz->player1);
        if (old_points!=this->ui->wyswietlacz->get_points()){
            old_points=this->ui->wyswietlacz->get_points();
            this->addToLogs(old_points);
        }
        this->ui->wyswietlacz->update();
        }








        qv_d0.append(double(this->ui->wyswietlacz->historia.back()[0])/1000);
        qv_d1.append(double(this->ui->wyswietlacz->historia.back()[1]));
        qv_d2.append(double(this->ui->wyswietlacz->historia.back()[2]));
        qv_d3.append(double(this->ui->wyswietlacz->historia.back()[3]));
        qv_d4.append(double(this->ui->wyswietlacz->historia.back()[4]));
        qv_d5.append(double(this->ui->wyswietlacz->historia.back()[5]));
        ui->plotwidget->graph(0)->addData(qv_d0,qv_d1,true);
        ui->plotwidget->graph(1)->addData(qv_d0,qv_d2,true);
        ui->plotwidget->graph(1)->addData(qv_d0,qv_d2,true);
        ui->plotwidget->graph(2)->addData(qv_d0,qv_d3,true);
        ui->plotwidget->graph(3)->addData(qv_d0,qv_d4,true);
        ui->plotwidget->graph(4)->addData(qv_d0,qv_d5,true);

            if (qv_d0.back()<=30)ui->plotwidget->xAxis->setRange(0,qv_d0.back());

            ui->plotwidget->replot();
            ui->plotwidget->update();



            //wprowadzenie danych pomiarowych z urządzenia do grafu
        if (mode==1){//jeżeli jest połączenie
            this->ui->progressBar_1->setValue(double(this->ui->wyswietlacz->pom1)*100/64+50);
            this->ui->progressBar_2->setValue(double(this->ui->wyswietlacz->pom2)*100/64+50);
            this->ui->progressBar_3->setValue(double(this->ui->wyswietlacz->pom3)*100/64+50);

            qv_d6.append(double(this->ui->wyswietlacz->pom1)*100/64+50);
            qv_d7.append(double(this->ui->wyswietlacz->pom2)*100/64+50);
            qv_d8.append(double(this->ui->wyswietlacz->pom3)*100/64+50);


                ui->plotwidget->graph(5)->addData(qv_d0,qv_d6,true);
                ui->plotwidget->graph(6)->addData(qv_d0,qv_d7,true);
                ui->plotwidget->graph(7)->addData(qv_d0,qv_d8,true);


        }
        update();


    } else {
        QWidget::timerEvent(event);
    }


}























/*
uint8_t crc8( uint8_t *addr, uint8_t len) {

    uint8_t bitarray[408];
    uint8_t i,j,t1,t2,tc1,tc2;
    uint8_t crc=0;
    uint8_t key[8];
    uint16_t size = 0;
    uint8_t temp[50];
    uint8_t data[50];// Tablica przechowujaca wysylana wiadomosc.
      //rozłożenie ciągu data na poszczególne bity
      for (i=0;i<size;i++){
        temp[i]=data[i];

        for (j=0;j<8;j++){
            t1=7+8*i-j;
            t2=(data[i]>>j)%2;
            bitarray[t1]=t2;
        }
      }
      //wyznaczenie klucza
        for (j=0;j<8;j++){
            t1=7-j;			//idąc od najmniej znaczącego bitu
            t2=(len>>j)%2;	//wyciągamy resztę z dzielenia przeez 2
            key[t1]=t2;		//wpisujemy do tabeli
            if (t2==1)	tc1=j;//jeżeli bit był dodatni to zwiększamy rząd klucza
            //tc1=rząd klucza(liczba bitów klucza z przykładu)
        }


        //wyznaczanei reszty z dzielenia wyrazu(całego pakietu danych) przez klucz
        for (i=0;i<len*8;i++){		//iteracja po całym wyrazie
            if(bitarray[i]==1)		//jeżeli aktualny bit jest 1
                for(j=0;j<tc1;j++){	//iteracja po długości wyrazu (tyle znaków ile ma klucz)
                    if (bitarray[i+j]==key[j]) bitarray[i+j]=0;	else bitarray[i+j]=1;	//XOR
                }
        }

        //wyznaczanie wartości sumy kontrolnej
        for (i=0;i<tc1-1;i++)
            tc2=2*tc2 + bitarray[len*8+i];
crc=tc2;

   return crc;
}

*/
