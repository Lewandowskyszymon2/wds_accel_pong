#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*!
 * \file
 * \brief Plik nagłówkowy z definicją klasy mainwindow
 */

#include <QMainWindow>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDateTime>
#include <QPainter>
#include <QTime>
#include <qbasictimer.h>
#include "qcustomplot/qcustomplot.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
/*!
 * \brief Klasa okna głównego
 *
 * Klasa odpowiedzialna za obsługę sygnałów przychodzących do programu (wciśniięcia klawiszy wewnątrz głównego okna programu, odbiór i parsowanie danych z urządzenia zewnętrznego)
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief Domyślny konstruktor klasy mainwindow
     * \param [in] parent -- wskaźnik na rodzica
     */
    MainWindow(QWidget *parent = nullptr);

    /*!
     * \brief Domyślny destruktor klasy MainWindow
     */
    ~MainWindow();

    /*!
     * \brief Metoda przekierowywująca komunikaty do okna logów
     * \param  [in] message -- treść wiadomości
     */
    void addToLogs(QString message);
    QString old_points;
    int starttime;
    int counter=0;
private slots:

    void on_pushButton_clicked();
    void on_okButton_clicked();
    /*!
     * \brief on_CloseButton_clicked
     * Slot obsługujący kliknięcie klawisza zamykania progranu
     */
    void on_CloseButton_clicked();
    /*!
     * \brief on_searchButton_clicked
     * Obsługa klawisza uruchamiającego wyszukiwanie podłączonych urządzeń
     */
    void on_searchButton_clicked();
    /*!
     * \brief on_connectButton_clicked
     * Obsługa klawisza rozpoczynającego połączenie z urządzeniem zewnętrznym
     */
    void on_connectButton_clicked();
    /*!
     * \brief on_disconnectButton_clicked
     * Obsługa klawisza zamykającego transmisję z urządzeniem zewnętrznym
     */
    void on_disconnectButton_clicked();

    /*!
     * \brief Wykonanie procedury rysowania gui
     * \param [in] event
     *
     */
    void paintEvent(QPaintEvent *event);
    /*!
     * \brief Odczyt sygnałów od urządzenia zewnętrznego
     */
    void readFromPort();



private:
    /*!
     * \brief ui
     * Ui głównego okna
     */
    Ui::MainWindow *ui;

    /*!
     * \brief m_serial
     * Wskaźnik na port do obsługi komunikacji
     */
    QSerialPort *m_serial = nullptr;
    /*!
     * \brief data
     * Tablica potrzebna do parsowania danych z urządzenia zewnętrznego
     */
    int data[4];

    /*!
     * \brief mode
     * Typ rozgrywki
     */
    int mode=0;
    /*!
     * \brief timer
     * Timer odpowiedizalny za krok gry i funkcji rysowania grafu
     */
    QBasicTimer timer;
protected:

    /*!
     * \brief timerEvent
     * \param event
     * Event wyzwalający krok gry i rysowanie grafu
     */
    void timerEvent(QTimerEvent *event);
};

#endif // MAINWINDOW_H
